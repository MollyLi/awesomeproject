'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';

class Wrapper extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.line}>
          {this.props.onBack && <TouchableOpacity style={styles.touchArea} onPress={this.props.onBack}>
            <Image style={styles.backBtn} source={require('./img/navBackGreen.png')} />
          </TouchableOpacity>}
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 20,
    backgroundColor: 'white'
  },
  line: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#ccc',
    height: 44,
  },
  touchArea: {
    position: 'absolute',
    left: 0,
    top: 0,
    padding: 10
  },
  backBtn: {
    resizeMode: 'cover',
    width: 18,
    height: 18,
  },
  title: {
    fontSize: 18,
    marginTop: 10,
  }
});


export default Wrapper;