'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

// {
//   "title": "The Basics - Networking",
//   "description": "Your app fetched this from a remote endpoint!",
//   "movies": [
//     { "title": "Star Wars", "releaseYear": "1977"},
//     { "title": "Back to the Future", "releaseYear": "1985"},
//     { "title": "The Matrix", "releaseYear": "1999"},
//     { "title": "Inception", "releaseYear": "2010"},
//     { "title": "Interstellar", "releaseYear": "2014"}
//   ]
// }

class FetchDemo extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	movies: [{title: ''}]
	  };
	}
  componentDidMount() {
    this.getMoviesFromApiAsync();
  }
  getMoviesFromApiAsync() {
    fetch('http://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ 
        	movies: responseJson.movies
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    return (
      <View>
      	{this.state.movies.map((item, idx)=>{
      		return (
      			<View key={idx}>
      				<Text>{item.title} {item.releaseYear}</Text>
      			</View>
      			);
      	})}
      </View>
    );
  }
}

const styles = StyleSheet.create({

});


export default FetchDemo;