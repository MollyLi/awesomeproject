'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Navigator,
} from 'react-native';

import Wrapper from './Wrapper';
import FixedDimensionsBasics from './FixedDimensionsBasics';
import JustifyContentBasics from './JustifyContentBasics';
import ListViewDemo from './ListViewDemo';
import LotsOfStyles from './LotsOfStyles';


class Root extends Component {
  _onBack(route, navigator) {
    if (route.index > 0) {
      navigator.pop();
    }
  }
  _onForward(nextRoute, navigator) {
    //console.log(nextRoute); 
    navigator.push(nextRoute);
  }
  render() {
    return (
        <Navigator
          initialRoute={{ title: 'My Initial Scene', index: 0, component: ListViewDemo}}
          renderScene={(route, navigator) => {
            const Comp = route.component;
            if(route.index > 0){
              return ( //第二頁之後有箭頭
                <Wrapper title={route.title} onBack={() => this._onBack(route, navigator)}>
                  <Comp />
                </Wrapper> );
            }else{ // 第一頁
              return (
                <Wrapper title={route.title}>
                  <Comp onForward={(nextRoute) => {this._onForward(nextRoute, navigator)}} />
                </Wrapper> );
            }
          }}
        />
    );
  }
}

const styles = StyleSheet.create({

});


export default Root;