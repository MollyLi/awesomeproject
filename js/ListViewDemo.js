'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  ListView,
  TouchableOpacity,
} from 'react-native';

import LotsOfStyles from './LotsOfStyles';
import FixedDimensionsBasics from './FixedDimensionsBasics';
import JustifyContentBasics from './JustifyContentBasics';
import AlignItemsBasics from './AlignItemsBasics';
import FetchDemo from './FetchDemo';

const data = [
  {
    index: 1,
    title: 'LotsOfStyles12',
    component: LotsOfStyles
  },
  {
    index: 1,
    title: 'FixedDimensionsBasics',
    component: FixedDimensionsBasics
  },
  {
    index: 1,
    title: 'JustifyContentBasics',
    component: JustifyContentBasics
  },
  {
    index: 1,
    title: 'AlignItemsBasics',
    component: AlignItemsBasics
  },
    {
    index: 1,
    title: 'FetchDemo',
    component: FetchDemo
  },
  {
    index: 1,
    title: 'FixedDimensionsBasics',
    component: FixedDimensionsBasics
  },
  {
    index: 1,
    title: 'JustifyContentBasics',
    component: JustifyContentBasics
  },
  {
    index: 1,
    title: 'AlignItemsBasics',
    component: AlignItemsBasics
  }
];

class ListViewDemo extends Component {
  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      dataSource: ds.cloneWithRows(data),
    };
  }
  _renderRow(rowData) {
    console.log(rowData);
    return (
    <TouchableOpacity style={styles.listBox} onPress={() => this.props.onForward(rowData)}>
      <Text>{rowData.title}</Text>
    </TouchableOpacity>);
  }
  render() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={(rowData) => this._renderRow(rowData)}
      />
    );
  }
}

const styles = StyleSheet.create({
  listBox: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    borderBottomWidth: 1,
    borderColor: '#FFF',
    backgroundColor: '#ddd'
  }
});


export default ListViewDemo;